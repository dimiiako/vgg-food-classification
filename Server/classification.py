from __future__ import print_function
from __future__ import division

import argparse
import tensorflow as tf
import localization
import cv2
import numpy as np 
import os

from keras.preprocessing import image
from keras.models import load_model


def classification(image_path, labels_path):

    img = image.load_img(image_path, target_size=(224, 224))
    img_tensor = image_preprocess(img)

    with graph3.as_default():
        cropped_image = localize(image_path)

    with graph.as_default():
        foodNonFood = model_binary.predict(img_tensor)
        print(foodNonFood) 

    if foodNonFood <= 0.5:
       
       # if food is identified, continue with next step
        cropped_image_tensor = image_preprocess(cropped_image)
        labels = localization.get_classes(labels_path)

        with graph2.as_default():
            classes = model_class.predict(img_tensor)
            cropped_classes = model_class.predict(cropped_image_tensor)
            prob = classes[0][np.argmax(classes)]
            cropped_prob = cropped_classes[0][np.argmax(cropped_classes)]
        
            # pick the decision with the highest probability
            if prob >= cropped_prob:
                label = str(labels[np.argmax(classes)])
                probability = str(prob*100)
                print(label + ": " + probability)
                return classes
            else:
                label = str(labels[np.argmax(cropped_classes)])
                probability = str(cropped_prob*100)
                print(label + ": " + probability)
                return cropped_classes

    else:
        print("image provided does not contain food")
        return None

def image_preprocess(img):
    img_tensor = image.img_to_array(img)
    img_tensor = np.expand_dims(img_tensor, axis=0)  
    img_tensor /= 255.
    return img_tensor

def localize(image_path):
    cropped_image = localization.localization(image_path)
    cv2.imwrite('cropped.jpg', cropped_image)

    cropped_img = image.load_img('cropped.jpg', target_size=(224, 224))
    return cropped_img


if __name__ == '__main__':
    # argument parser
    parser = argparse.ArgumentParser(description='GMAP food segmentation')
    parser.add_argument('--image', type=str,
                        help='the file path of the input image')
    args = parser.parse_args()

    graph = tf.get_default_graph()
    model_binary = load_model('model_binary.hdf5')
    graph2 = tf.get_default_graph()
    model_class = load_model('model_class.hdf5')
    graph3 = tf.get_default_graph()
    model_detection = load_model('model_detection.hdf5')

    # loading of the image
    image_path = args.image
    labels_path = 'classes.txt'
    labels = localization.get_classes(labels_path)

    result = classification(image_path, labels_path)  
    if result is None:
        print("Not a food image provided")
    else:
        label = str(labels[np.argmax(result)])

        probability = str(result[0][np.argmax(result)]*100)
        print(label + ": " + probability)