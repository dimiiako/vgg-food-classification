from __future__ import print_function
from __future__ import division


import argparse
import keras
from keras.regularizers import l2
from keras import backend as K
from keras.models import Sequential, Model, load_model
from keras.layers import Input
from keras.layers.core import Flatten, Dense, Dropout, Activation
from keras.layers.convolutional import Conv2D, MaxPooling2D, ZeroPadding2D
from keras.layers.pooling import GlobalMaxPooling2D, GlobalAveragePooling2D
from keras.optimizers import SGD, Adam
from keras.preprocessing.image import load_img, img_to_array, ImageDataGenerator
from keras.preprocessing import image
from keras.utils import np_utils
from keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions

import matplotlib.pyplot as plt
import matplotlib.image as img
import numpy as np
import cv2

def load_image(img_path, dimentions, rescale=1. / 255):
    img = load_img(img_path, target_size=dimentions)
    x = img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x *= rescale # rescale the same as when trained

    return x
def get_classes(file_path):
    with open(file_path) as f:
        classes = f.read().splitlines()

    return classes
def create_model(num_classes, dropout, shape):


    base_model = VGG16(include_top=False, input_tensor=Input(shape=shape))
    model = base_model.output

    model = ZeroPadding2D((1,1))(model)
    model = Conv2D(1024, (3, 3), activation='relu')(model)
    model = GlobalMaxPooling2D()(model)
    model = Dropout(dropout)(model)
    predictions = Dense(num_classes, activation='softmax')(model)
    model_final = Model(inputs=base_model.input, outputs=predictions)

    return model_final
def load_model(model_final, weights_path, shape, dropout):
   model_final = create_model(101, dropout, shape)
   model_final.load_weights(weights_path)

   return model_final



def get_classmap_numpy(model, img, label):

    _ , width, height, _ = img.shape

    # Get the 1024 input weights to softmax
    class_weights = model.layers[-1].get_weights()[0]
    final_conv_layer = model.layers[-4]
    get_output = K.function([model.layers[0].input, keras.backend.learning_phase()], [final_conv_layer.output, model.layers[-1].output])
    [conv_outputs, predictions] = get_output([img, 0])
    conv_outputs = conv_outputs[0,:,:,:]

    #create the class activation map
    cam = np.zeros(dtype=np.float32, shape=conv_outputs.shape[0:2])
    for i,w in enumerate(class_weights[:,label]):
        cam += w*conv_outputs[:,:,i]


    cam = cv2.resize(cam, (height, width))

    return cam

def plot_cam3(original_image, cam):

    height, width ,_ = original_image.shape
    cam_norm = cam / np.max(cam)
    cam_resize = cv2.resize(cam_norm, (width, height))
    cam_resize = cam_resize*255
    cam_resize = cam_resize.astype(np.uint8)
    heatmap = cv2.applyColorMap(cam_resize, cv2.COLORMAP_JET)
    heatmap[np.where(cam_resize < 0.2)] = 0
    
    # threshold image for pixels where value > max(cam_resize)*0.2
    _, thresh = cv2.threshold(cam_resize,round(255*0.2), 255, cv2.THRESH_BINARY)
    print(thresh.shape)

    # create contours
    _, contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours_poly = [None]*len(contours)
    boundRect = [None]*len(contours)

    for i, c in enumerate(contours):
        contours_poly[i] = cv2.approxPolyDP(c, 3, True)
        boundRect[i] = cv2.boundingRect(contours_poly[i])
    
    drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3), dtype=np.uint8)

    for i in range(len(contours)):
        color = (100,100,100)
        cv2.drawContours(drawing, contours_poly, i, color)
        cv2.rectangle(drawing, (int(boundRect[i][0]), int(boundRect[i][1])), \
          (int(boundRect[i][0]+boundRect[i][2]), int(boundRect[i][1]+boundRect[i][3])), color, 2)

    cropped_image = original_image[boundRect[0][0]:boundRect[0][0]+boundRect[0][2], \
            boundRect[i][1]:boundRect[i][1]+boundRect[i][3]]


    # x, y, w, h = cv2.boundingRect(contours)
    # cv2.rectangle(original_image, (x, y), (x+w, y+h), (0, 255, 0), 2)
    # cv2.drawContours(original_image, contours, -1, (255, 255, 0), 1)
    # cv2.imshow("contours", original_image)

    return cropped_image

def localization(image_file_path):

    # initialization of the model
    shape = (224, 224, 3)
    dropout=0.5
    trained_model = create_model(101, dropout, shape)
    trained_model.load_weights('model_detection.hdf5')

    # loading of the image
    image_path = image_file_path
    original_image = cv2.imread(image_path)

    # load the image in keras form and predict label
    image = load_image(image_path , shape[:2])
    preds = trained_model.predict(image)

    # get network activation for image and class
    cam = get_classmap_numpy(trained_model, image, np.argmax(preds))
    
    # return cropped image
    cropped_image = plot_cam3(original_image, cam)
    cv2.resize(cropped_image, dsize=(224, 224))
    return cropped_image



